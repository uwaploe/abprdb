// Abprdb stores the contents of an ABPR data store in a Bolt Database file
package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"time"

	"bitbucket.org/uwaploe/abprdata"
	bolt "go.etcd.io/bbolt"
	"google.golang.org/protobuf/proto"
)

const Usage = `Usage: abprdb [options] datadir dbfile

`

var Version = "dev"
var BuildDate = "unknown"

type myTime time.Time

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	tFrom myTime
)

func (t *myTime) String() string {
	return fmt.Sprint(*t)
}

// Flexible ISO8601-ish date/time parser. It handles the following formats
// (missing least-significant values are assumed to be zero):
//
//    YYYY-mm-ddTHH:MM:SS-ZZZZ
//    YYYY-mm-ddTHH:MM:SS   (UTC assumed)
//    YYYY-mm-ddTHH:MM
//    YYYY-mm-dd
//
func (mt *myTime) Set(dt string) error {
	t, err := time.Parse("2006-01-02T15:04:05-0700", dt)
	if err != nil {
		t, err = time.Parse("2006-01-02T15:04:05", dt)
		if err != nil {
			t, err = time.Parse("2006-01-02T15:04", dt)
			if err != nil {
				t, err = time.Parse("2006-01-02", dt)
			}
		}
	}
	*mt = myTime(t)
	return err
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.Var(&tFrom, "since", "extract data records on or newer than specified date")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func addRecords(db *bolt.DB, bucket string, ch <-chan *abprdata.Record) error {
	err := db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(bucket))
		return err
	})
	if err != nil {
		return err
	}

	err = db.Update(func(tx *bolt.Tx) error {
		for rec := range ch {
			key := []byte(time.Unix(rec.Time, 0).UTC().Format(time.RFC3339))
			val, err := proto.Marshal(rec)
			if err != nil {
				return err
			}
			b := tx.Bucket([]byte(bucket))
			if err := b.Put(key, val); err != nil {
				return err
			}
		}
		return nil
	})

	return err
}

func main() {
	args := parseCmdLine()
	if len(args) < 2 {
		flag.Usage()
		os.Exit(1)
	}

	store := abprdata.NewDataStore(args[0])
	db, err := bolt.Open(args[1], 0644, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		log.Fatal(err)
	}

	dr := store.Range()
	tstart := time.Time(tFrom)
	if tstart.IsZero() {
		tstart = time.Unix(dr.Start, 0).UTC()
	}

	for _, minute := range []int{0, 15, 30, 45} {
		view := store.NewView(minute)
		ch := view.Walk(context.Background(), tstart, 0)
		if err := addRecords(db, "stored-data", ch); err != nil {
			log.Fatal(err)
		}
	}
}
