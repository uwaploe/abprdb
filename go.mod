module bitbucket.org/uwaploe/abprdb

go 1.15

require (
	bitbucket.org/uwaploe/abprdata v0.9.9
	go.etcd.io/bbolt v1.3.5
	google.golang.org/protobuf v1.25.0
)
